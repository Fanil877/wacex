if ($('#map').length > 0) {
    ymaps.ready(init);
  }
  
  function init() {
    var location = ymaps.geolocation;
    var inputSearch = new ymaps.control.SearchControl({
      options: {
        // РџСѓСЃС‚СЊ СЌР»РµРјРµРЅС‚ СѓРїСЂР°РІР»РµРЅРёСЏ Р±СѓРґРµС‚
        // РІ РІРёРґРµ РїРѕРёСЃРєРѕРІРѕР№ СЃС‚СЂРѕРєРё.
        size: 'large',
        // Р’РєР»СЋС‡РёРј РІРѕР·РјРѕР¶РЅРѕСЃС‚СЊ РёСЃРєР°С‚СЊ
        // РЅРµ С‚РѕР»СЊРєРѕ С‚РѕРїРѕРЅРёРјС‹, РЅРѕ Рё РѕСЂРіР°РЅРёР·Р°С†РёРё.
        provider: 'yandex#search'
      }
    }),
  
      // Р”РѕР±Р°РІРёРј РїРѕРёСЃРєРѕРІСѓСЋ СЃС‚СЂРѕРєСѓ РЅР° РєР°СЂС‚Сѓ. 
      myMap = new ymaps.Map('map', {
        zoom: 10,
        center: [55.76, 27.64],
        controls: [inputSearch, 'zoomControl']
      });
  
    // РџРѕР»СѓС‡РµРЅРёРµ РјРµСЃС‚РѕРїРѕР»РѕР¶РµРЅРёСЏ Рё Р°РІС‚РѕРјР°С‚РёС‡РµСЃРєРѕРµ РѕС‚РѕР±СЂР°Р¶РµРЅРёРµ РµРіРѕ РЅР° РєР°СЂС‚Рµ.
    location.get({
      mapStateAutoApply: true
    })
      .then(
        function (result) {
          // РџРѕР»СѓС‡РµРЅРёРµ РјРµСЃС‚РѕРїРѕР»РѕР¶РµРЅРёСЏ РїРѕР»СЊР·РѕРІР°С‚РµР»СЏ.
          var userAddress = result.geoObjects.get(0).properties.get('text');
          var userCoodinates = result.geoObjects.get(0).geometry.getCoordinates();
          // РџСЂРѕРїРёС€РµРј РїРѕР»СѓС‡РµРЅРЅС‹Р№ Р°РґСЂРµСЃ РІ Р±Р°Р»СѓРЅРµ.
          result.geoObjects.get(0).properties.set({
            balloonContentBody: 'РђРґСЂРµСЃ: ' + userAddress +
              '<br/>РљРѕРѕСЂРґРёРЅР°С‚С‹:' + userCoodinates
          });
          myMap.geoObjects.add(result.geoObjects)
        },
        function (err) {
          console.log('РћС€РёР±РєР°: ' + err)
        }
      );
  
    // РЎРѕР·РґР°РµРј РіРµРѕРѕР±СЉРµРєС‚ СЃ С‚РёРїРѕРј РіРµРѕРјРµС‚СЂРёРё "РўРѕС‡РєР°".
    myGeoObject = new ymaps.GeoObject({
      // РћРїРёСЃР°РЅРёРµ РіРµРѕРјРµС‚СЂРёРё.
      geometry: {
        type: "Point",
        coordinates: [55.8, 37.8]
      },
    }, {
        // РћРїС†РёРё.
        // РРєРѕРЅРєР° РјРµС‚РєРё Р±СѓРґРµС‚ СЂР°СЃС‚СЏРіРёРІР°С‚СЊСЃСЏ РїРѕРґ СЂР°Р·РјРµСЂ РµРµ СЃРѕРґРµСЂР¶РёРјРѕРіРѕ.
        preset: 'twirl#redStretchyIcon',
  
      }),
  
      myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
  
      }, {
          // РћРїС†РёРё.
          // РќРµРѕР±С…РѕРґРёРјРѕ СѓРєР°Р·Р°С‚СЊ РґР°РЅРЅС‹Р№ С‚РёРї РїСЂРѕРµРєС‚Р°.
          iconLayout: 'default#image',
          // РЎРІРѕС‘ РёР·РѕР±СЂР°Р¶РµРЅРёРµ РёРєРѕРЅРєРё РјРµС‚РєРё.
          iconImageHref: 'img/maps.svg',
          // Р Р°Р·РјРµСЂС‹ РјРµС‚РєРё.
          iconImageSize: [37, 37],
          // РЎРјРµС‰РµРЅРёРµ Р»РµРІРѕРіРѕ РІРµСЂС…РЅРµРіРѕ СѓРіР»Р° РёРєРѕРЅРєРё РѕС‚РЅРѕСЃРёС‚РµР»СЊРЅРѕ
          // РµС‘ "РЅРѕР¶РєРё" (С‚РѕС‡РєРё РїСЂРёРІСЏР·РєРё).
          iconImageOffset: [-27, -2]
        });
  
    myPlacemark2 = new ymaps.Placemark([55.76, 37.78], {
      // РЎРІРѕР№СЃС‚РІР°.
  
    }, {
        // РћРїС†РёРё.
        iconLayout: 'default#image',
        // РЎРІРѕС‘ РёР·РѕР±СЂР°Р¶РµРЅРёРµ РёРєРѕРЅРєРё РјРµС‚РєРё.
        iconImageHref: 'img/maps.svg',
        iconImageSize: [37, 37],
        // РЎРјРµС‰РµРЅРёРµ Р»РµРІРѕРіРѕ РІРµСЂС…РЅРµРіРѕ СѓРіР»Р° РёРєРѕРЅРєРё РѕС‚РЅРѕСЃРёС‚РµР»СЊРЅРѕ
        // РµС‘ "РЅРѕР¶РєРё" (С‚РѕС‡РєРё РїСЂРёРІСЏР·РєРё).
        iconImageOffset: [-27, -2]
      });
  
    myPlacemark3 = new ymaps.Placemark([55.9198471, 37.7654985], {
      // РЎРІРѕР№СЃС‚РІР°.
  
    }, {
        // РћРїС†РёРё.
        iconLayout: 'default#image',
        // РЎРІРѕС‘ РёР·РѕР±СЂР°Р¶РµРЅРёРµ РёРєРѕРЅРєРё РјРµС‚РєРё.
        iconImageHref: 'img/maps.svg',
        iconImageSize: [37, 37],
        // РЎРјРµС‰РµРЅРёРµ Р»РµРІРѕРіРѕ РІРµСЂС…РЅРµРіРѕ СѓРіР»Р° РёРєРѕРЅРєРё РѕС‚РЅРѕСЃРёС‚РµР»СЊРЅРѕ
        // РµС‘ "РЅРѕР¶РєРё" (С‚РѕС‡РєРё РїСЂРёРІСЏР·РєРё).
        iconImageOffset: [-27, -2]
      });
  
    myMap.behaviors.disable('scrollZoom');
  
    if ($(window).width() < 768) {
    } else {
      // change functionality for larger screens
    }
    myMap.geoObjects.add(myPlacemark)
      .add(myPlacemark2).add(myPlacemark3)
  }