jQuery(document).ready(function () {

  $('.navigation__burger').on('click', function (e) {
    e.preventDefault;
    $(this).toggleClass('navigation__burger--active');
    $('.menu-popup').toggleClass('menu-popup--active');
  })

  jQuery(window).scroll(hideBlock);
  function hideBlock() {
    jQuery('.menu-popup').removeClass('menu-popup--active');
    jQuery('.navigation__burger').removeClass('navigation__burger--active')
  }

  jQuery('.menu-popup__link').on('click', function (e) {
    e.preventDefault;
    jQuery('.navigation__burger').removeClass('navigation__burger--active');
    jQuery('.menu-popup').removeClass('menu-popup--active');
  });

  jQuery('.menu-popup__btn-1').on('click', function (e) {
    e.preventDefault;
    jQuery('.navigation__burger').removeClass('navigation__burger--active');
    jQuery('.menu-popup').removeClass('menu-popup--active');
  });

  jQuery('.menu-popup__btn-2').on('click', function (e) {
    e.preventDefault;
    jQuery('.navigation__burger').removeClass('navigation__burger--active');
    jQuery('.menu-popup').removeClass('menu-popup--active');
  });
})

const md = 991
const sm = 767

$(document).ready(function () {
  var amount = 199;

  $('#bronze').on('click', function () {
    amount = 199;
  })
  $('#silver').on('click', function () {
    amount = 299;
  })
  $('#gold').on('click', function () {
    amount = 499;
  })

  paypal.Buttons({
    createOrder: function (data, actions) {
      return actions.order.create({
        purchase_units: [{
          amount: {
            value: amount,
          }
        }]
      });
    },
    onApprove: function (data, actions) {
      return actions.order.capture().then(function (details) {
        window.location = 'application.html';
      });
    }
  }).render('#paymentWindow .paypal-form');


  $("#point").change(function () {
    if ($(this).val() == 'point1') {
      $('#point2').addClass('disabled');
      $('#point3').addClass('disabled');
    }
    if ($(this).val() == 'point2') {
      $('#point2').removeClass('disabled');
      $('#point3').addClass('disabled');
    }
    if ($(this).val() == 'point3') {
      $('#point2').removeClass('disabled');
      $('#point3').removeClass('disabled');
    }
  });
  $('.btn-3').on('click', function (e) {
    e.preventDefault();
    $('.btn-3').removeClass('chosen');
    $(this).addClass('chosen');
  });
  $('#invoice').click(function () {
    $('.btn-send').show();
    $('.btn-next').hide();
  });
  $('#card').click(function () {
    $('.btn-next').show();
    $('.btn-send').hide();
  });

  $('.btn-send').on('click', function (e) {
    e.preventDefault();
    $('.invoice-sent').removeClass('hide');
    $('.invoice-sent').addClass('show');
    $('html, body').css({
      overflow: 'hidden',
      height: '100%'
    });
    function redirect() {
      window.location.href = '../application.html';
    }
    setTimeout(redirect, 5000);
  });
  $('.btn-next').on('click', function (e) {
    e.preventDefault();
    $('.invoice-next').removeClass('hide');
    $('.invoice-next').addClass('show');
    $('html, body').css({
      overflow: 'hidden',
      height: '100%'
    });
  });
  $('.invoice-next__btn-back').on('click', function (e) {
    e.preventDefault();
    $('.invoice-next').removeClass('show');
    $('.invoice-next').addClass('hide');
    $('html, body').css({
      overflow: 'auto',
      height: 'auto'
    });
  });

})